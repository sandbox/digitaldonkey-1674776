<?php
/**
 * @file
 * responsive_switchtheme_feature.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function responsive_switchtheme_feature_default_fe_block_settings() {
  $export = array();

  // adaptivetheme_subtheme
  $theme = array();

  $theme['switchtheme-switch_form'] = array(
    'module' => 'switchtheme',
    'delta' => 'switch_form',
    'theme' => 'adaptivetheme_subtheme',
    'status' => '1',
    'weight' => '0',
    'region' => 'sidebar_second',
    'custom' => '0',
    'visibility' => '0',
    'pages' => '',
    'title' => '',
    'cache' => '1',
  );

  $export['adaptivetheme_subtheme'] = $theme;

  // corolla
  $theme = array();

  $theme['switchtheme-switch_form'] = array(
    'module' => 'switchtheme',
    'delta' => 'switch_form',
    'theme' => 'corolla',
    'status' => '1',
    'weight' => '0',
    'region' => 'sidebar_second',
    'custom' => '0',
    'visibility' => '0',
    'pages' => '',
    'title' => '',
    'cache' => '1',
  );

  $export['corolla'] = $theme;

  // oursasson_subtheme
  $theme = array();

  $theme['switchtheme-switch_form'] = array(
    'module' => 'switchtheme',
    'delta' => 'switch_form',
    'theme' => 'oursasson_subtheme',
    'status' => '1',
    'weight' => '0',
    'region' => 'sidebar_second',
    'custom' => '0',
    'visibility' => '0',
    'pages' => '',
    'title' => '',
    'cache' => '1',
  );

  $export['oursasson_subtheme'] = $theme;

  // pixture_reloaded
  $theme = array();

  $theme['switchtheme-switch_form'] = array(
    'module' => 'switchtheme',
    'delta' => 'switch_form',
    'theme' => 'pixture_reloaded',
    'status' => '1',
    'weight' => '0',
    'region' => 'sidebar_second',
    'custom' => '0',
    'visibility' => '0',
    'pages' => '',
    'title' => '',
    'cache' => '1',
  );

  $export['pixture_reloaded'] = $theme;

  // pop_customtheme
  $theme = array();

  $theme['switchtheme-switch_form'] = array(
    'module' => 'switchtheme',
    'delta' => 'switch_form',
    'theme' => 'pop_customtheme',
    'status' => '1',
    'weight' => '0',
    'region' => 'sidebar_second',
    'custom' => '0',
    'visibility' => '0',
    'pages' => '',
    'title' => '',
    'cache' => '1',
  );

  $export['pop_customtheme'] = $theme;

  // seven
  $theme = array();

  $theme['switchtheme-switch_form'] = array(
    'module' => 'switchtheme',
    'delta' => 'switch_form',
    'theme' => 'seven',
    'status' => 0,
    'weight' => '0',
    'region' => '',
    'custom' => '0',
    'visibility' => '0',
    'pages' => '',
    'title' => '',
    'cache' => '1',
  );

  $export['seven'] = $theme;

  // sky
  $theme = array();

  $theme['switchtheme-switch_form'] = array(
    'module' => 'switchtheme',
    'delta' => 'switch_form',
    'theme' => 'sky',
    'status' => '1',
    'weight' => '0',
    'region' => 'sidebar_second',
    'custom' => '0',
    'visibility' => '0',
    'pages' => '',
    'title' => '',
    'cache' => '1',
  );

  $export['sky'] = $theme;

  // stark
  $theme = array();

  $theme['switchtheme-switch_form'] = array(
    'module' => 'switchtheme',
    'delta' => 'switch_form',
    'theme' => 'stark',
    'status' => '1',
    'weight' => '0',
    'region' => 'sidebar_second',
    'custom' => '0',
    'visibility' => '0',
    'pages' => '',
    'title' => '',
    'cache' => '1',
  );

  $export['stark'] = $theme;

  // unser_omega
  $theme = array();

  $theme['switchtheme-switch_form'] = array(
    'module' => 'switchtheme',
    'delta' => 'switch_form',
    'theme' => 'unser_omega',
    'status' => '1',
    'weight' => '0',
    'region' => 'sidebar_second',
    'custom' => '0',
    'visibility' => '0',
    'pages' => '',
    'title' => '',
    'cache' => '1',
  );

  $export['unser_omega'] = $theme;

  // unser_zen
  $theme = array();

  $theme['switchtheme-switch_form'] = array(
    'module' => 'switchtheme',
    'delta' => 'switch_form',
    'theme' => 'unser_zen',
    'status' => '1',
    'weight' => '0',
    'region' => 'sidebar_second',
    'custom' => '0',
    'visibility' => '0',
    'pages' => '',
    'title' => '',
    'cache' => '1',
  );

  $export['unser_zen'] = $theme;

  $theme_default = variable_get('theme_default', 'garland');
  $themes = list_themes();
  foreach ($export as $theme_key => $settings) {
    if ($theme_key != $theme_default && empty($themes[$theme_key]->status)) {
      unset($export[$theme_key]);
    }
  }
  return $export;
}
