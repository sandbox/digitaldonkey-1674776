<?php
/**
 * @file
 * responsive_switchtheme_feature.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function responsive_switchtheme_feature_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer switch.
  $permissions['administer switch'] = array(
    'name' => 'administer switch',
    'roles' => array(),
    'module' => 'switchtheme',
  );

  // Exported permission: select different theme.
  $permissions['select different theme'] = array(
    'name' => 'select different theme',
    'roles' => array(),
    'module' => 'switchtheme',
  );

  // Exported permission: switch theme.
  $permissions['switch theme'] = array(
    'name' => 'switch theme',
    'roles' => array(
      0 => 'anonymous user',
      1 => 'authenticated user',
    ),
    'module' => 'switchtheme',
  );

  return $permissions;
}
